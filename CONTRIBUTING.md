# Contribute

Community made patches, bug reports and contributions are always welcome!

When contributing please ensure you follow the guidelines below so that we can
keep on top of things.

## Getting Started

* Submit a ticket for your issue, assuming one does not already exist.
  * Raise it on our [Issue Tracker](https://gitlab.com/widgitlabs/arma3/arma3-project-template/-/issues)
  * Clearly describe the issue including steps to reproduce the bug.
  * Make sure you fill in the earliest version that you know has the issue.

## Making Changes

* Fork the repository on GitLab
* Make the changes to your forked repository
  * Ensure you stick to the [black](https://black.readthedocs.io/en/stable/) coding standards
  * Please ensure that if you add or edit a method, you update the documentation as necessary
* When committing, reference your issue (if present) and include a note about
  the fix
* If possible, and applicable, please also add/update unit tests for your changes
* Push the changes to your fork and submit a merge request to the 'main' branch
  of the repository

At this point you're waiting on us to merge your request. We'll review all
merge requests, and make suggestions and changes if necessary.

### Additional Resources

* [General GitLab Documentation](https://docs.gitlab.com/ee/user/)
* [GitLab Merge Request Documentation](https://docs.gitlab.com/ee/user/project/merge_requests/index.html#merge-requests)
