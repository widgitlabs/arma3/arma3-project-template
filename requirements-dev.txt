black==23.3.0
flake8==6.0.0
mypy==1.4.1
pylint==2.17.4
pytest==7.4.0
sqflint==0.3.2